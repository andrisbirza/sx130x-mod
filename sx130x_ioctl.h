#ifndef SX130X_IOCTL_H
#define SX130X_IOCTL_H

#include <linux/types.h>

struct sx130x_conf_board 
{
    bool    lorawan_public; 		/*!> Enable ONLY for *public* networks using the LoRa MAC protocol */
    __u8 	clksrc;         		/*!> Index of RF chain which provides clock to concentrator */
};

#define SX130X_CONF_SET_BOARD		_IOW('E', 0x01, struct sx130x_conf_board)

int sx130x_ioctl_conf_set_board(unsigned int cmd, unsigned long arg);


#endif