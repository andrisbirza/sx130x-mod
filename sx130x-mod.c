#include <linux/module.h>
#include <linux/fs.h>
#include <linux/gpio/consumer.h>
#include <linux/spi/spi.h>
#include <linux/miscdevice.h>
#include <linux/of.h>

#include "sx130x_ioctl.h"

static struct spi_device *_spi_dev;
static struct gpio_desc *reset;

static int sx130x_open(struct inode *inode, struct file *file)
{
	pr_info("sx130x_open() is called\n");
	return 0;
}

static int sx130x_close(struct inode *inode, struct file *file)
{
	pr_info("sx130x_close() is called\n");
	return 0;
}

static long sx130x_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	pr_info("sx130x_ioctl() is called\n");

	switch(cmd)
	{
		case SX130X_CONF_SET_BOARD:
			pr_info("sx130x_ioctl: SX130X_CONF_SET_BOARD\n");
			return sx130x_ioctl_conf_set_board(cmd, arg);
	}

	return -EINVAL;
}

static ssize_t sx130x_read(struct file *file, char __user *buffer, size_t count, loff_t ppos)
{
	int ret;
	struct spi_message msg;
	struct spi_transfer xfer;
	
	// struct spidev_data *spidev;

	char tx_buf[] = {
		0xDE, 0xAD, 0xBE, 0xEF
	};

	char rx_buf[4] = {0,};

	pr_info("sx130x_read() is called\n");

	xfer.tx_buf = tx_buf;
	xfer.rx_buf = rx_buf;
	xfer.len = sizeof(tx_buf);
	xfer.bits_per_word = 8;
	xfer.speed_hz = 100000;

	spi_message_init(&msg);
	spi_message_add_tail(&xfer, &msg);

	gpiod_set_value(reset, 0);

	ret = spi_sync(_spi_dev, &msg);
	pr_info("sx130x_read() ret value:%d\n", ret);

	gpiod_set_value(reset, 1);

	return 0;
}


static const struct file_operations sx130x_fops = {
	.owner = THIS_MODULE,
	.open = sx130x_open,
	.read = sx130x_read,
	.release = sx130x_close,
	.unlocked_ioctl = sx130x_ioctl,
};

static struct miscdevice sx130x_miscdevice = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "sx130x",
	.fops = &sx130x_fops,
}; 

static int sx130x_probe(struct spi_device *spi_dev)
{
	int ret;
	struct device *dev;

	pr_info("sx130x_probe() is called\n");

	dev = &spi_dev->dev;
	reset = gpiod_get_index(dev, "reset", 0, GPIOD_OUT_HIGH);

	_spi_dev = spi_dev;

	ret = misc_register(&sx130x_miscdevice);
	if(ret != 0)
	{
		pr_err("sx130x could not register misc device\n");
		return ret;
	}

	pr_info("sx130x: got_minor %i\n", sx130x_miscdevice.minor);

	return 0;
}

static int sx130x_remove(struct spi_device *spi_dev)
{
	pr_info("sx130x_remove() is called\n");
	return 0;
}

static const struct of_device_id sx130x_of_ids[] = {
	{.compatible = "andris,sx130x"},
	{},
};
MODULE_DEVICE_TABLE(of, sx130x_of_ids);

static struct spi_driver sx130x_driver = {
	.probe = sx130x_probe,
	.remove = sx130x_remove,
	.driver = {
		.name = "sx130x",
		.of_match_table = sx130x_of_ids,
		.owner = THIS_MODULE,
	}
};

static int __init sx130x_init(void)
{
	pr_info("sx130x_init() is called.\n");
	return spi_register_driver(&sx130x_driver);
}
module_init(sx130x_init);

static void __exit sx130x_exit(void)
{
	pr_info("sx130x_exit() is called\n");
	return spi_unregister_driver(&sx130x_driver);
}
module_exit(sx130x_exit);


MODULE_LICENSE("GPL");
