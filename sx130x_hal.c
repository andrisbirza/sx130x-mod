#include "sx1301_hal.h"
// #include "gw_hal/gw_reg.h"
// #include "gw_hal/gw_radio.h"
// #include "gw_hal/gw_delay.h"

// #include "math.h"       /* pow, cell */
// #include "string.h"     /* memcpy */
// #include "stdio.h"
// #include "stdbool.h"

// #include "configs/config_sx1301.h"
// #include "logger.h"

#define LGW_RF_RX_BANDWIDTH_125KHZ  925000      /* for 125KHz channels */
#define LGW_RF_RX_BANDWIDTH_250KHZ  1000000     /* for 250KHz channels */
#define LGW_RF_RX_BANDWIDTH_500KHZ  1100000     /* for 500KHz channels */

#define TX_START_DELAY_DEFAULT  1497 /* Calibrated value for 500KHz BW and notch filter disabled */

#define MCU_ARB             0
#define MCU_AGC             1
#define MCU_ARB_FW_BYTE     8192 /* size of the firmware IN BYTES (= twice the number of 14b words) */
#define MCU_AGC_FW_BYTE     8192 /* size of the firmware IN BYTES (= twice the number of 14b words) */
#define FW_VERSION_ADDR     0x20 /* Address of firmware version in data memory */
#define FW_VERSION_CAL      2 /* Expected version of calibration firmware */
#define FW_VERSION_AGC      4 /* Expected version of AGC firmware */
#define FW_VERSION_ARB      1 /* Expected version of arbiter firmware */

#define TX_METADATA_NB      16
#define RX_METADATA_NB      16

#define AGC_CMD_WAIT        16
#define AGC_CMD_ABORT       17

#define MIN_LORA_PREAMBLE   6
#define STD_LORA_PREAMBLE   8
#define MIN_FSK_PREAMBLE    3
#define STD_FSK_PREAMBLE    5

#define RSSI_MULTI_BIAS     -35 /* difference between "multi" modem RSSI offset and "stand-alone" modem RSSI offset */
#define RSSI_FSK_POLY_0     60 /* polynomiam coefficients to linearize FSK RSSI */
#define RSSI_FSK_POLY_1     1.5351
#define RSSI_FSK_POLY_2     0.003

#define SET_PPM_ON(bw,dr)   (((bw == BW_125KHZ) && ((dr == DR_LORA_SF11) || (dr == DR_LORA_SF12))) || ((bw == BW_250KHZ) && (dr == DR_LORA_SF12)))
#define IF_HZ_TO_REG(f)     (f << 5)/15625

#define FW_CALLOW_FN		"callow.bin"
#define FW_AGC_FN			"agc.bin"
#define FW_ARB_FN			"arb.bin"


static bool lgw_is_started = false;

static bool lorawan_public = false;
static uint8_t rf_clkout = 0;

static struct lgw_tx_gain_lut_s txgain_lut = {
    .size = 2,
    .lut[0] = {
        .dig_gain = 0,
        .pa_gain = 2,
        .dac_gain = 3,
        .mix_gain = 10,
        .rf_power = 14
    },
    .lut[1] = {
        .dig_gain = 0,
        .pa_gain = 3,
        .dac_gain = 3,
        .mix_gain = 14,
        .rf_power = 27
    }};

static bool rf_enable[LGW_RF_CHAIN_NB];
static uint32_t rf_rx_freq[LGW_RF_CHAIN_NB];
static float rf_rssi_offset[LGW_RF_CHAIN_NB];
static bool rf_tx_enable[LGW_RF_CHAIN_NB];
static enum lgw_radio_type_e rf_radio_type[LGW_RF_CHAIN_NB];

static bool if_enable[LGW_IF_CHAIN_NB];
static bool if_rf_chain[LGW_IF_CHAIN_NB];
static int32_t if_freq[LGW_IF_CHAIN_NB];

const uint8_t ifmod_config[LGW_IF_CHAIN_NB] = LGW_IFMODEM_CONFIG;

static uint8_t lora_rx_bw;
static uint8_t lora_rx_sf;
static bool lora_rx_ppm_offset;

static uint8_t fsk_rx_bw;
static uint32_t fsk_rx_dr;
static uint8_t fsk_sync_word_size = 3;
static uint64_t fsk_sync_word = 0xC194C1;

static uint8_t lora_multi_sfmask[LGW_MULTI_NB];

/* TX I/Q imbalance coefficients for mixer gain = 8 to 15 */
static int8_t cal_offset_a_i[8]; /* TX I offset for radio A */
static int8_t cal_offset_a_q[8]; /* TX Q offset for radio A */
static int8_t cal_offset_b_i[8]; /* TX I offset for radio B */
static int8_t cal_offset_b_q[8]; /* TX Q offset for radio B */

static int stm32_delay = 0;

static int32_t gw_hal_bw_getval(int x);

static int load_firmware(uint8_t target, uint8_t *firmware, uint16_t size);
static int load_firmware_sd(uint8_t target, char *filename, uint16_t size);

static uint16_t lgw_get_tx_start_delay(bool tx_notch_enable, uint8_t bw);
static int lgw_abort_tx(void);
static void gw_constant_adjust(void); 

int sx130x_hal_board_setconf(struct lgw_conf_board_s *conf)
{
	if(lgw_is_started)
	{
		return LGW_HAL_ERROR;
	}

	lorawan_public = conf->lorawan_public;
	rf_clkout = conf->clksrc;

	return LGW_HAL_SUCCESS;
}

int sx130x_hal_rxrf_setconf(uint8_t rf_chain, struct lgw_conf_rxrf_s *conf)
{
	// if(lgw_is_started)
	// {
	// 	return LGW_HAL_ERROR;
	// }

	// if(rf_chain >= LGW_RF_CHAIN_NB)
	// {
	// 	return LGW_HAL_ERROR;
	// }

	// if ((conf->type != LGW_RADIO_TYPE_SX1255) && (conf->type != LGW_RADIO_TYPE_SX1257)) 
	// {
	// 	return LGW_HAL_ERROR;
	// }

	// rf_enable[rf_chain] = conf->enable;
    // rf_rx_freq[rf_chain] = conf->freq_hz;
    // rf_rssi_offset[rf_chain] = conf->rssi_offset;
    // rf_radio_type[rf_chain] = conf->type;
	// rf_tx_enable[rf_chain] = conf->tx_enable;
	
	return LGW_HAL_SUCCESS;
}

int sx130x_hal_rxif_setconf(uint8_t if_chain, struct lgw_conf_rxif_s *conf)
{
	// int32_t bw_hz;
    // uint32_t rf_rx_bandwidth;

	// if(lgw_is_started == true) 
	// {
    //     return LGW_HAL_ERROR;
    // }

	// if(if_chain >= LGW_IF_CHAIN_NB) 
	// {
    //     return LGW_HAL_ERROR;
    // }

    // /* if chain is disabled, don't care about most parameters */
	// if(conf->enable == false) 
	// {
    //     if_enable[if_chain] = false;
    //     if_freq[if_chain] = 0;
    //     return LGW_HAL_SUCCESS;
    // }

	// if(conf->rf_chain >= LGW_RF_CHAIN_NB) 
	// {
    //     return LGW_HAL_ERROR;
	// }
	
    // switch (conf->bandwidth) {
    //     case BW_250KHZ:
    //         rf_rx_bandwidth = LGW_RF_RX_BANDWIDTH_250KHZ;
    //         break;
    //     case BW_500KHZ:
    //         rf_rx_bandwidth = LGW_RF_RX_BANDWIDTH_500KHZ;
    //         break;
    //     default:
    //         rf_rx_bandwidth = LGW_RF_RX_BANDWIDTH_125KHZ;
    //         break;
    // }

    // bw_hz = gw_hal_bw_getval(conf->bandwidth);
	// if ((conf->freq_hz + ((bw_hz == -1) ? LGW_REF_BW : bw_hz) / 2) > ((int32_t)rf_rx_bandwidth / 2)) 
	// {
    //     return LGW_HAL_ERROR;
	// } 
	// else if ((conf->freq_hz - ((bw_hz == -1) ? LGW_REF_BW : bw_hz) / 2) < -((int32_t)rf_rx_bandwidth / 2)) 
	// {
    //     return LGW_HAL_ERROR;
    // }

	// switch(ifmod_config[if_chain])
	// {
    //     case IF_LORA_STD:
	// 		if(conf->bandwidth == BW_UNDEFINED) 
	// 		{
    //             conf->bandwidth = BW_250KHZ;
	// 		}
			
	// 		if(conf->datarate == DR_UNDEFINED)
	// 		{
    //             conf->datarate = DR_LORA_SF9;
    //         }
            
	// 		if(!IS_LORA_BW(conf->bandwidth))
	// 		{
    //             return LGW_HAL_ERROR;
	// 		}
			
	// 		if(!IS_LORA_STD_DR(conf->datarate))
	// 		{
    //             return LGW_HAL_ERROR;
	// 		}
			
    //         if_enable[if_chain] = conf->enable;
    //         if_rf_chain[if_chain] = conf->rf_chain;
	// 		if_freq[if_chain] = conf->freq_hz;
			
    //         lora_rx_bw = conf->bandwidth;
    //         lora_rx_sf = (uint8_t)(DR_LORA_MULTI & conf->datarate);
	// 		if(SET_PPM_ON(conf->bandwidth, conf->datarate)) 
	// 		{
    //             lora_rx_ppm_offset = true;
	// 		}
	// 		else 
	// 		{
    //             lora_rx_ppm_offset = false;
    //         }

    //         break;

    //     case IF_LORA_MULTI:
	// 		if(conf->bandwidth == BW_UNDEFINED) 
	// 		{
    //             conf->bandwidth = BW_125KHZ;
	// 		}
			
	// 		if(conf->datarate == DR_UNDEFINED) 
	// 		{
    //             conf->datarate = DR_LORA_MULTI;
	// 		}
			
	// 		if(conf->bandwidth != BW_125KHZ) 
	// 		{
    //             return LGW_HAL_ERROR;
	// 		}
			
	// 		if(!IS_LORA_MULTI_DR(conf->datarate)) 
	// 		{
    //             return LGW_HAL_ERROR;
    //         }

    //         if_enable[if_chain] = conf->enable;
    //         if_rf_chain[if_chain] = conf->rf_chain;
    //         if_freq[if_chain] = conf->freq_hz;
    //         lora_multi_sfmask[if_chain] = (uint8_t)(DR_LORA_MULTI & conf->datarate);

    //         break;

    //     case IF_FSK_STD:
	// 		if(conf->bandwidth == BW_UNDEFINED) 
	// 		{
    //             conf->bandwidth = BW_250KHZ;
	// 		}
			
	// 		if(conf->datarate == DR_UNDEFINED) 
	// 		{
    //             conf->datarate = 64000;
    //         }
            
	// 		if(!IS_FSK_BW(conf->bandwidth)) 
	// 		{
    //             return LGW_HAL_ERROR;
	// 		}
			
	// 		if(!IS_FSK_DR(conf->datarate)) 
	// 		{
    //             return LGW_HAL_ERROR;
    //         }

    //         if_enable[if_chain] = conf->enable;
    //         if_rf_chain[if_chain] = conf->rf_chain;
    //         if_freq[if_chain] = conf->freq_hz;
    //         fsk_rx_bw = conf->bandwidth;
    //         fsk_rx_dr = conf->datarate;
	// 		if(conf->sync_word > 0) 
	// 		{
    //             fsk_sync_word_size = conf->sync_word_size;
    //             fsk_sync_word = conf->sync_word;
	// 		}
			
    //         break;

    //     default:
    //         return LGW_HAL_ERROR;
	// }
	
	return LGW_HAL_SUCCESS;
}

int sx130x_hal_start()
{
	// int i;
	// unsigned x;
	// uint8_t radio_select;
	// int32_t read_val;
	// uint8_t load_val;
    // uint8_t fw_version;
    // uint8_t cal_cmd;
    // uint16_t cal_time;
    // uint8_t cal_status;

	// uint64_t fsk_sync_word_reg;

	// if(gw_reg_connect() != LGW_REG_SUCCESS)
	// {
	// 	return LGW_HAL_ERROR;
	// }
	
	// if(gw_reg_soft_reset() != LGW_HAL_SUCCESS)
	// {
	// 	return LGW_HAL_ERROR;
	// }

	// // Gate Clocks
	// gw_reg_write(LGW_GLOBAL_EN, 0);
	// gw_reg_write(LGW_CLK32M_EN, 0);
	
	// // Switck on and reset radios
	// gw_reg_write(LGW_RADIO_A_EN, 1);
	// gw_reg_write(LGW_RADIO_B_EN, 1);

	// gw_delay_ms(500);

	// gw_reg_write(LGW_RADIO_RST, 1);
    // gw_delay_ms(5);
	// gw_reg_write(LGW_RADIO_RST, 0);
	
	// // Setup radios
	// if(gw_radio_setup_sx125x(0, rf_clkout, rf_enable[0], rf_radio_type[0], rf_rx_freq[0]) != LGW_REG_SUCCESS)
	// {
	// 	return LGW_HAL_ERROR;
	// }

	// if(gw_radio_setup_sx125x(1, rf_clkout, rf_enable[1], rf_radio_type[1], rf_rx_freq[1]) != LGW_REG_SUCCESS)
	// {
	// 	return LGW_HAL_ERROR;
	// }

	// /* gives AGC control of GPIOs to enable Tx external digital filter */
	// gw_reg_write(LGW_GPIO_MODE, 31);
	// gw_reg_write(LGW_GPIO_SELECT_OUTPUT, 2);

	// /* Enable clocks */
	// gw_reg_write(LGW_GLOBAL_EN, 1);
	// gw_reg_write(LGW_CLK32M_EN, 1);

	// cal_cmd = 0;
    // cal_cmd |= rf_enable[0] ? 0x01 : 0x00; /* Bit 0: Calibrate Rx IQ mismatch compensation on radio A */
    // cal_cmd |= rf_enable[1] ? 0x02 : 0x00; /* Bit 1: Calibrate Rx IQ mismatch compensation on radio B */
    // cal_cmd |= (rf_enable[0] && rf_tx_enable[0]) ? 0x04 : 0x00; /* Bit 2: Calibrate Tx DC offset on radio A */
    // cal_cmd |= (rf_enable[1] && rf_tx_enable[1]) ? 0x08 : 0x00; /* Bit 3: Calibrate Tx DC offset on radio B */
	// cal_cmd |= 0x10; /* Bit 4: 0: calibrate with DAC gain=2, 1: with DAC gain=3 (use 3) */
	

	// switch (rf_radio_type[0]) 
	// { /* we assume that there is only one radio type on the board */
    //     case LGW_RADIO_TYPE_SX1255:
    //         cal_cmd |= 0x20; /* Bit 5: 0: SX1257, 1: SX1255 */
    //         break;
    //     case LGW_RADIO_TYPE_SX1257:
    //         cal_cmd |= 0x00; /* Bit 5: 0: SX1257, 1: SX1255 */
    //         break;
    //     default:
    //         break;
	// }
	
	// cal_cmd |= 0x00; /* Bit 6-7: Board type 0: ref, 1: FPGA, 3: board X */
	// cal_time = 2300; /* measured between 2.1 and 2.2 sec, because 1 TX only */
	
	// /* Load the calibration firmware  */
	// if(load_firmware_sd(MCU_AGC, FW_CALLOW_FN, MCU_AGC_FW_BYTE) != LGW_HAL_SUCCESS)
	// {
	// 	return LGW_HAL_ERROR;
	// }

	// gw_reg_write(LGW_FORCE_HOST_RADIO_CTRL, 0); /* gives to AGC MCU the control of the radios */
    // gw_reg_write(LGW_RADIO_SELECT, cal_cmd); /* send calibration configuration word */
	// gw_reg_write(LGW_MCU_RST_1, 0);
	
	// /* Check firmware version */
	// gw_reg_write(LGW_DBG_AGC_MCU_RAM_ADDR, FW_VERSION_ADDR);
	// gw_reg_read(LGW_DBG_AGC_MCU_RAM_DATA, &read_val);
	// fw_version = (uint8_t)read_val;
	// if (fw_version != FW_VERSION_CAL) 
	// {	
	// 	return LGW_HAL_ERROR;
	// }

	// gw_reg_write(LGW_PAGE_REG, 3); /* Calibration will start on this condition as soon as MCU can talk to concentrator registers */
	// gw_reg_write(LGW_EMERGENCY_FORCE_HOST_CTRL, 0); /* Give control of concentrator registers to MCU */
	
	// gw_delay_ms(cal_time); /* Wait for end of calibration */
	// gw_reg_write(LGW_EMERGENCY_FORCE_HOST_CTRL, 1); /* Take back control */
	
	// gw_reg_read(LGW_MCU_AGC_STATUS, &read_val);
	// cal_status = (uint8_t)read_val;
	
	//     /*
    //     bit 7: calibration finished
    //     bit 0: could access SX1301 registers
    //     bit 1: could access radio A registers
    //     bit 2: could access radio B registers
    //     bit 3: radio A RX image rejection successful
    //     bit 4: radio B RX image rejection successful
    //     bit 5: radio A TX DC Offset correction successful
    //     bit 6: radio B TX DC Offset correction successful
    // */
    // if ((cal_status & 0x81) != 0x81) {
    //     // DEBUG_PRINTF("ERROR: CALIBRATION FAILURE (STATUS = 0x%X)\n", cal_status);
    //     return LGW_HAL_ERROR;
	// }
	
    // if (rf_enable[0] && ((cal_status & 0x02) == 0)) {
    //     // c_printf(LOG_ALL, "ERROR: calibration could not access radio A\n");
    //     return LGW_HAL_ERROR;
    // }
    // if (rf_enable[1] && ((cal_status & 0x04) == 0)) {
    //     // c_printf(LOG_ALL, "ERROR: calibration could not access radio B\n");
    //     return LGW_HAL_ERROR;
	// }
	
    // if (rf_enable[0] && ((cal_status & 0x08) == 0)) {
	// 	return LGW_HAL_ERROR;
    //     // c_printf(LOG_ALL, "WARNING: problem in calibration of radio A for image rejection\n");
    // }
    // if (rf_enable[1] && ((cal_status & 0x10) == 0)) {
	// 	return LGW_HAL_ERROR;
    //     // c_printf(LOG_ALL, "WARNING: problem in calibration of radio B for image rejection\n");
    // }
    // if (rf_enable[0] && rf_tx_enable[0] && ((cal_status & 0x20) == 0)) {
	// 	return LGW_HAL_ERROR;
    //     // c_printf(LOG_ALL, "WARNING: problem in calibration of radio A for TX DC offset\n");
    // }
    // if (rf_enable[1] && rf_tx_enable[1] && ((cal_status & 0x40) == 0)) {
	// 	return LGW_HAL_ERROR;
    //     // c_printf(LOG_ALL, "WARNING: problem in calibration of radio B for TX DC offset\n");
	// }
	
	// for(i = 0; i < 8; ++i) 
	// {
    //     gw_reg_write(LGW_DBG_AGC_MCU_RAM_ADDR, 0xA0 + i);
    //     gw_reg_read(LGW_DBG_AGC_MCU_RAM_DATA, &read_val);
    //     cal_offset_a_i[i] = (int8_t)read_val;
    //     gw_reg_write(LGW_DBG_AGC_MCU_RAM_ADDR, 0xA8 + i);
    //     gw_reg_read(LGW_DBG_AGC_MCU_RAM_DATA, &read_val);
    //     cal_offset_a_q[i] = (int8_t)read_val;
    //     gw_reg_write(LGW_DBG_AGC_MCU_RAM_ADDR, 0xB0 + i);
    //     gw_reg_read(LGW_DBG_AGC_MCU_RAM_DATA, &read_val);
    //     cal_offset_b_i[i] = (int8_t)read_val;
    //     gw_reg_write(LGW_DBG_AGC_MCU_RAM_ADDR, 0xB8 + i);
    //     gw_reg_read(LGW_DBG_AGC_MCU_RAM_DATA, &read_val);
    //     cal_offset_b_q[i] = (int8_t)read_val;
	// }
	
	// gw_constant_adjust();

	// /* Sanity check for RX frequency */
	// if (rf_rx_freq[0] == 0) 
	// {
	// 	return LGW_HAL_ERROR;
	// }
	
	// /* Freq-to-time-drift calculation */
	// x = 4096000000 / (rf_rx_freq[0] >> 1); /* dividend: (4*2048*1000000) >> 1, rescaled to avoid 32b overflow */
	// x = ( x > 63 ) ? 63 : x; /* saturation */
	// gw_reg_write(LGW_FREQ_TO_TIME_DRIFT, x); /* default 9 */
	
	// x = 4096000000 / (rf_rx_freq[0] >> 3); /* dividend: (16*2048*1000000) >> 3, rescaled to avoid 32b overflow */
	// x = ( x > 63 ) ? 63 : x; /* saturation */
	// gw_reg_write(LGW_MBWSSF_FREQ_TO_TIME_DRIFT, x); /* default 36 */
	
	// /* configure LoRa 'multi' demodulators aka. LoRa 'sensor' channels (IF0-3) */
	// radio_select = 0; /* IF mapping to radio A/B (per bit, 0=A, 1=B) */
	// for(i=0; i<LGW_MULTI_NB; ++i) {
	// 	radio_select += (if_rf_chain[i] == 1 ? 1 << i : 0); /* transform bool array into binary word */
	// }
	
	// gw_reg_write(LGW_IF_FREQ_0, IF_HZ_TO_REG(if_freq[0])); /* default -384 */
    // gw_reg_write(LGW_IF_FREQ_1, IF_HZ_TO_REG(if_freq[1])); /* default -128 */
    // gw_reg_write(LGW_IF_FREQ_2, IF_HZ_TO_REG(if_freq[2])); /* default 128 */
    // gw_reg_write(LGW_IF_FREQ_3, IF_HZ_TO_REG(if_freq[3])); /* default 384 */
    // gw_reg_write(LGW_IF_FREQ_4, IF_HZ_TO_REG(if_freq[4])); /* default -384 */
    // gw_reg_write(LGW_IF_FREQ_5, IF_HZ_TO_REG(if_freq[5])); /* default -128 */
    // gw_reg_write(LGW_IF_FREQ_6, IF_HZ_TO_REG(if_freq[6])); /* default 128 */
    // gw_reg_write(LGW_IF_FREQ_7, IF_HZ_TO_REG(if_freq[7])); /* default 384 */

    // gw_reg_write(LGW_CORR0_DETECT_EN, (if_enable[0] == true) ? lora_multi_sfmask[0] : 0); /* default 0 */
    // gw_reg_write(LGW_CORR1_DETECT_EN, (if_enable[1] == true) ? lora_multi_sfmask[1] : 0); /* default 0 */
    // gw_reg_write(LGW_CORR2_DETECT_EN, (if_enable[2] == true) ? lora_multi_sfmask[2] : 0); /* default 0 */
    // gw_reg_write(LGW_CORR3_DETECT_EN, (if_enable[3] == true) ? lora_multi_sfmask[3] : 0); /* default 0 */
    // gw_reg_write(LGW_CORR4_DETECT_EN, (if_enable[4] == true) ? lora_multi_sfmask[4] : 0); /* default 0 */
    // gw_reg_write(LGW_CORR5_DETECT_EN, (if_enable[5] == true) ? lora_multi_sfmask[5] : 0); /* default 0 */
    // gw_reg_write(LGW_CORR6_DETECT_EN, (if_enable[6] == true) ? lora_multi_sfmask[6] : 0); /* default 0 */
    // gw_reg_write(LGW_CORR7_DETECT_EN, (if_enable[7] == true) ? lora_multi_sfmask[7] : 0); /* default 0 */

    // gw_reg_write(LGW_PPM_OFFSET, 0x60); /* as the threshold is 16ms, use 0x60 to enable ppm_offset for SF12 and SF11 @125kHz*/

    // gw_reg_write(LGW_CONCENTRATOR_MODEM_ENABLE, 1); /* default 0 */

	// /* configure LoRa 'stand-alone' modem (IF8) */
	// gw_reg_write(LGW_IF_FREQ_8, IF_HZ_TO_REG(if_freq[8])); /* MBWSSF modem (default 0) */
	// if (if_enable[8] == true) 
	// {
	// 	gw_reg_write(LGW_MBWSSF_RADIO_SELECT, if_rf_chain[8]);
	// 	switch(lora_rx_bw) 
	// 	{
	// 		case BW_125KHZ: gw_reg_write(LGW_MBWSSF_MODEM_BW, 0); break;
	// 		case BW_250KHZ: gw_reg_write(LGW_MBWSSF_MODEM_BW, 1); break;
	// 		case BW_500KHZ: gw_reg_write(LGW_MBWSSF_MODEM_BW, 2); break;
	// 		default:
	// 			return LGW_HAL_ERROR;
	// 	}
	// 	switch(lora_rx_sf)
	// 	{
	// 		case DR_LORA_SF7: gw_reg_write(LGW_MBWSSF_RATE_SF, 7); break;
	// 		case DR_LORA_SF8: gw_reg_write(LGW_MBWSSF_RATE_SF, 8); break;
	// 		case DR_LORA_SF9: gw_reg_write(LGW_MBWSSF_RATE_SF, 9); break;
	// 		case DR_LORA_SF10: gw_reg_write(LGW_MBWSSF_RATE_SF, 10); break;
	// 		case DR_LORA_SF11: gw_reg_write(LGW_MBWSSF_RATE_SF, 11); break;
	// 		case DR_LORA_SF12: gw_reg_write(LGW_MBWSSF_RATE_SF, 12); break;
	// 		default:
	// 			return LGW_HAL_ERROR;
	// 	}
	// 	gw_reg_write(LGW_MBWSSF_PPM_OFFSET, lora_rx_ppm_offset); /* default 0 */	
	// 	gw_reg_write(LGW_MBWSSF_MODEM_ENABLE, 1); /* default 0 */
	// } 
	// else 
	// {
	// 	gw_reg_write(LGW_MBWSSF_MODEM_ENABLE, 0);
	// }

	// /* configure FSK modem (IF9) */
	// gw_reg_write(LGW_IF_FREQ_9, IF_HZ_TO_REG(if_freq[9])); /* FSK modem, default 0 */
	// gw_reg_write(LGW_FSK_PSIZE, fsk_sync_word_size-1);
	// gw_reg_write(LGW_FSK_TX_PSIZE, fsk_sync_word_size-1);
	// fsk_sync_word_reg = fsk_sync_word << (8 * (8 - fsk_sync_word_size));
	// gw_reg_write(LGW_FSK_REF_PATTERN_LSB, (uint32_t)(0xFFFFFFFF & fsk_sync_word_reg));
	// gw_reg_write(LGW_FSK_REF_PATTERN_MSB, (uint32_t)(0xFFFFFFFF & (fsk_sync_word_reg >> 32)));
	// if (if_enable[9] == true) 
	// {
	// 	gw_reg_write(LGW_FSK_RADIO_SELECT, if_rf_chain[9]);
	// 	gw_reg_write(LGW_FSK_BR_RATIO, LGW_XTAL_FREQU/fsk_rx_dr); /* setting the dividing ratio for datarate */
	// 	gw_reg_write(LGW_FSK_CH_BW_EXPO, fsk_rx_bw);
	// 	gw_reg_write(LGW_FSK_MODEM_ENABLE, 1); /* default 0 */
	// } 
	// else 
	// {
	// 	gw_reg_write(LGW_FSK_MODEM_ENABLE, 0);
	// }

	// /* Load firmware */
	// // load_firmware(MCU_ARB, arb_firmware, MCU_ARB_FW_BYTE);
	// if(load_firmware_sd(MCU_ARB, FW_ARB_FN, MCU_ARB_FW_BYTE) != LGW_HAL_SUCCESS)
	// {
	// 	return LGW_HAL_ERROR;
	// }
	
	// // load_firmware(MCU_AGC, agc_firmware, MCU_AGC_FW_BYTE);
	// if(load_firmware_sd(MCU_AGC, FW_AGC_FN, MCU_AGC_FW_BYTE) != LGW_HAL_SUCCESS)
	// {
	// 	return LGW_HAL_ERROR;
	// }

	// /* gives the AGC MCU control over radio, RF front-end and filter gain */
	// gw_reg_write(LGW_FORCE_HOST_RADIO_CTRL, 0);
	// gw_reg_write(LGW_FORCE_HOST_FE_CTRL, 0);
	// gw_reg_write(LGW_FORCE_DEC_FILTER_GAIN, 0);
	
	// /* Get MCUs out of reset */
	// gw_reg_write(LGW_RADIO_SELECT, 0); /* MUST not be = to 1 or 2 at firmware init */
	// gw_reg_write(LGW_MCU_RST_0, 0);
	// gw_reg_write(LGW_MCU_RST_1, 0);

	// /* Check firmware version */
	// gw_reg_write(LGW_DBG_AGC_MCU_RAM_ADDR, FW_VERSION_ADDR);
	// gw_reg_read(LGW_DBG_AGC_MCU_RAM_DATA, &read_val);
	// fw_version = (uint8_t)read_val;
	// if (fw_version != FW_VERSION_AGC) {
	// 	return LGW_HAL_ERROR;
	// }
	// gw_reg_write(LGW_DBG_ARB_MCU_RAM_ADDR, FW_VERSION_ADDR);
	// gw_reg_read(LGW_DBG_ARB_MCU_RAM_DATA, &read_val);
	// fw_version = (uint8_t)read_val;
	// if (fw_version != FW_VERSION_ARB) {
	// 	return LGW_HAL_ERROR;
	// }

	// gw_delay_ms(1);
	
	// gw_reg_read(LGW_MCU_AGC_STATUS, &read_val);
	// if (read_val != 0x10) {
	// 	return LGW_HAL_ERROR;
	// }

	// /* Update Tx gain LUT and start AGC */
	// for (i = 0; i < txgain_lut.size; ++i) {
	// 	gw_reg_write(LGW_RADIO_SELECT, AGC_CMD_WAIT); /* start a transaction */
	// 	gw_delay_ms(1);
	// 	load_val = txgain_lut.lut[i].mix_gain + (16 * txgain_lut.lut[i].dac_gain) + (64 * txgain_lut.lut[i].pa_gain);
	// 	gw_reg_write(LGW_RADIO_SELECT, load_val);
	// 	gw_delay_ms(1);
	// 	gw_reg_read(LGW_MCU_AGC_STATUS, &read_val);
	// 	if (read_val != (0x30 + i)) {
	// 		return LGW_HAL_ERROR;
	// 	}
	// }
	// /* As the AGC fw is waiting for 16 entries, we need to abort the transaction if we get less entries */
	// if (txgain_lut.size < TX_GAIN_LUT_SIZE_MAX) {
	// 	gw_reg_write(LGW_RADIO_SELECT, AGC_CMD_WAIT);
	// 	gw_delay_ms(1);
	// 	load_val = AGC_CMD_ABORT;
	// 	gw_reg_write(LGW_RADIO_SELECT, load_val);
	// 	gw_delay_ms(1);
	// 	gw_reg_read(LGW_MCU_AGC_STATUS, &read_val);
	// 	if (read_val != 0x30) {
	// 		return LGW_HAL_ERROR;
	// 	}
	// }

	// /* Load Tx freq MSBs (always 3 if f > 768 for SX1257 or f > 384 for SX1255 */
	// gw_reg_write(LGW_RADIO_SELECT, AGC_CMD_WAIT);
	// gw_delay_ms(1);
	// gw_reg_write(LGW_RADIO_SELECT, 3);
	// gw_delay_ms(1);
	// gw_reg_read(LGW_MCU_AGC_STATUS, &read_val);
	// if (read_val != 0x33) {
	// 	return LGW_HAL_ERROR;
	// }
		
	// /* Load chan_select firmware option */
	// gw_reg_write(LGW_RADIO_SELECT, AGC_CMD_WAIT);
	// gw_delay_ms(1);
	// gw_reg_write(LGW_RADIO_SELECT, 0);
	// gw_delay_ms(1);
	// gw_reg_read(LGW_MCU_AGC_STATUS, &read_val);
	// if (read_val != 0x30) {
	// 	return LGW_HAL_ERROR;
	// }
		
	// /* End AGC firmware init and check status */
	// gw_reg_write(LGW_RADIO_SELECT, AGC_CMD_WAIT);
	// gw_delay_ms(1);
	// gw_reg_write(LGW_RADIO_SELECT, radio_select); /* Load intended value of RADIO_SELECT */
	// gw_delay_ms(1);
	// gw_reg_read(LGW_MCU_AGC_STATUS, &read_val);
	// if (read_val != 0x40) {
	// 	return LGW_HAL_ERROR;
	// }
		
	// /* enable GPS event capture */
	// gw_reg_write(LGW_GPS_EN, 1);
		
	// /* */
	// // if (lbt_is_enabled() == true) {
	// // 	gw_delay_ms(8400);
	// // }
		
	// lgw_is_started = true;

	return LGW_HAL_SUCCESS;
}

int sx130x_hal_receive(uint8_t max_pkt, struct lgw_pkt_rx_s *pkt_data) 
{
    // int nb_pkt_fetch; /* loop variable and return value */
    // struct lgw_pkt_rx_s *p; /* pointer to the current structure in the struct array */
    // uint8_t buff[255+RX_METADATA_NB]; /* buffer to store the result of SPI read bursts */
    // unsigned sz; /* size of the payload, uses to address metadata */
    // int ifmod; /* type of if_chain/modem a packet was received by */
    // int stat_fifo; /* the packet status as indicated in the FIFO */
    // uint32_t raw_timestamp; /* timestamp when internal 'RX finished' was triggered */
    // uint32_t delay_x, delay_y, delay_z; /* temporary variable for timestamp offset calculation */
    // uint32_t timestamp_correction; /* correction to account for processing delay */
    // uint32_t sf, cr, bw_pow, crc_en, ppm; /* used to calculate timestamp correction */

    // /* check if the concentrator is running */
    // if (lgw_is_started == false) {
    //     return LGW_HAL_ERROR;
    // }

    // /* check input variables */
    // if ((max_pkt <= 0) || (max_pkt > LGW_PKT_FIFO_SIZE)) {
    //     return LGW_HAL_ERROR;
    // }

    // /* Initialize buffer */
    // memset (buff, 0, sizeof(buff));

    // /* iterate max_pkt times at most */
    // for (nb_pkt_fetch = 0; nb_pkt_fetch < max_pkt; ++nb_pkt_fetch) {

    //     /* point to the proper struct in the struct array */
    //     p = &pkt_data[nb_pkt_fetch];

    //     /* fetch all the RX FIFO data */
    //     gw_reg_read_burst(LGW_RX_PACKET_DATA_FIFO_NUM_STORED, buff, 5);
    //     /* 0:   number of packets available in RX data buffer */
    //     /* 1,2: start address of the current packet in RX data buffer */
    //     /* 3:   CRC status of the current packet */
    //     /* 4:   size of the current packet payload in byte */

    //     /* how many packets are in the RX buffer ? Break if zero */
    //     if (buff[0] == 0) {
    //         break; /* no more packets to fetch, exit out of FOR loop */
    //     }

    //     /* sanity check */
    //     if (buff[0] > LGW_PKT_FIFO_SIZE) {
    //         break;
    //     }

    //     p->size = buff[4];
    //     sz = p->size;
    //     stat_fifo = buff[3]; /* will be used later, need to save it before overwriting buff */

    //     /* get payload + metadata */
    //     gw_reg_read_burst(LGW_RX_DATA_BUF_DATA, buff, sz+RX_METADATA_NB);

    //     /* copy payload to result struct */
    //     memcpy((void *)p->payload, (void *)buff, sz);

	// 	// HAL_UART_Transmit(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size, uint32_t Timeout);
	// 	// printf("%.16s\n", p->payload);

    //     /* process metadata */
    //     p->if_chain = buff[sz+0];
    //     if (p->if_chain >= LGW_IF_CHAIN_NB) {
    //         break;
    //     }
    //     ifmod = ifmod_config[p->if_chain];

    //     p->rf_chain = (uint8_t)if_rf_chain[p->if_chain];
    //     p->freq_hz = (uint32_t)((int32_t)rf_rx_freq[p->rf_chain] + if_freq[p->if_chain]);
    //     p->rssi = (float)buff[sz+5] + rf_rssi_offset[p->rf_chain];

    //     if ((ifmod == IF_LORA_MULTI) || (ifmod == IF_LORA_STD)) {
    //         switch(stat_fifo & 0x07) {
    //             case 5:
    //                 p->status = STAT_CRC_OK;
    //                 crc_en = 1;
    //                 break;
    //             case 7:
    //                 p->status = STAT_CRC_BAD;
    //                 crc_en = 1;
    //                 break;
    //             case 1:
    //                 p->status = STAT_NO_CRC;
    //                 crc_en = 0;
    //                 break;
    //             default:
    //                 p->status = STAT_UNDEFINED;
    //                 crc_en = 0;
    //         }
    //         p->modulation = MOD_LORA;
    //         p->snr = ((float)((int8_t)buff[sz+2]))/4;
    //         p->snr_min = ((float)((int8_t)buff[sz+3]))/4;
    //         p->snr_max = ((float)((int8_t)buff[sz+4]))/4;
    //         if (ifmod == IF_LORA_MULTI) {
    //             p->bandwidth = BW_125KHZ; /* fixed in hardware */
    //         } else {
    //             p->bandwidth = lora_rx_bw; /* get the parameter from the config variable */
    //         }
    //         sf = (buff[sz+1] >> 4) & 0x0F;
    //         switch (sf) {
    //             case 7: p->datarate = DR_LORA_SF7; break;
    //             case 8: p->datarate = DR_LORA_SF8; break;
    //             case 9: p->datarate = DR_LORA_SF9; break;
    //             case 10: p->datarate = DR_LORA_SF10; break;
    //             case 11: p->datarate = DR_LORA_SF11; break;
    //             case 12: p->datarate = DR_LORA_SF12; break;
    //             default: p->datarate = DR_UNDEFINED;
    //         }
    //         cr = (buff[sz+1] >> 1) & 0x07;
    //         switch (cr) {
    //             case 1: p->coderate = CR_LORA_4_5; break;
    //             case 2: p->coderate = CR_LORA_4_6; break;
    //             case 3: p->coderate = CR_LORA_4_7; break;
    //             case 4: p->coderate = CR_LORA_4_8; break;
    //             default: p->coderate = CR_UNDEFINED;
    //         }

    //         /* determine if 'PPM mode' is on, needed for timestamp correction */
    //         if (SET_PPM_ON(p->bandwidth,p->datarate)) {
    //             ppm = 1;
    //         } else {
    //             ppm = 0;
    //         }

    //         /* timestamp correction code, base delay */
    //         if (ifmod == IF_LORA_STD) { /* if packet was received on the stand-alone LoRa modem */
    //             switch (lora_rx_bw) {
    //                 case BW_125KHZ:
    //                     delay_x = 64;
    //                     bw_pow = 1;
    //                     break;
    //                 case BW_250KHZ:
    //                     delay_x = 32;
    //                     bw_pow = 2;
    //                     break;
    //                 case BW_500KHZ:
    //                     delay_x = 16;
    //                     bw_pow = 4;
    //                     break;
    //                 default:
    //                     delay_x = 0;
    //                     bw_pow = 0;
    //             }
    //         } else { /* packet was received on one of the sensor channels = 125kHz */
    //             delay_x = 114;
    //             bw_pow = 1;
    //         }

    //         /* timestamp correction code, variable delay */
    //         if ((sf >= 6) && (sf <= 12) && (bw_pow > 0)) {
    //             if ((2*(sz + 2*crc_en) - (sf-7)) <= 0) { /* payload fits entirely in first 8 symbols */
    //                 delay_y = ( ((1<<(sf-1)) * (sf+1)) + (3 * (1<<(sf-4))) ) / bw_pow;
    //                 delay_z = 32 * (2*(sz+2*crc_en) + 5) / bw_pow;
    //             } else {
    //                 delay_y = ( ((1<<(sf-1)) * (sf+1)) + ((4 - ppm) * (1<<(sf-4))) ) / bw_pow;
    //                 delay_z = (16 + 4*cr) * (((2*(sz+2*crc_en)-sf+6) % (sf - 2*ppm)) + 1) / bw_pow;
    //             }
    //             timestamp_correction = delay_x + delay_y + delay_z;
    //         } else {
    //             timestamp_correction = 0;
    //         }

    //         /* RSSI correction */
    //         if (ifmod == IF_LORA_MULTI) {
    //             p->rssi -= RSSI_MULTI_BIAS;
    //         }

    //     } else if (ifmod == IF_FSK_STD) {
    //         switch(stat_fifo & 0x07) {
    //             case 5:
    //                 p->status = STAT_CRC_OK;
    //                 break;
    //             case 7:
    //                 p->status = STAT_CRC_BAD;
    //                 break;
    //             case 1:
    //                 p->status = STAT_NO_CRC;
    //                 break;
    //             default:
    //                 p->status = STAT_UNDEFINED;
    //                 break;
    //         }
    //         p->modulation = MOD_FSK;
    //         p->snr = -128.0;
    //         p->snr_min = -128.0;
    //         p->snr_max = -128.0;
    //         p->bandwidth = fsk_rx_bw;
    //         p->datarate = fsk_rx_dr;
    //         p->coderate = CR_UNDEFINED;
    //         timestamp_correction = ((uint32_t)680000 / fsk_rx_dr) - 20;

    //         /* RSSI correction */
    //         p->rssi = RSSI_FSK_POLY_0 + RSSI_FSK_POLY_1 * p->rssi + RSSI_FSK_POLY_2 * pow(p->rssi, 2);
    //     } else {
    //         p->status = STAT_UNDEFINED;
    //         p->modulation = MOD_UNDEFINED;
    //         p->rssi = -128.0;
    //         p->snr = -128.0;
    //         p->snr_min = -128.0;
    //         p->snr_max = -128.0;
    //         p->bandwidth = BW_UNDEFINED;
    //         p->datarate = DR_UNDEFINED;
    //         p->coderate = CR_UNDEFINED;
    //         timestamp_correction = 0;
    //     }

    //     raw_timestamp = (uint32_t)buff[sz+6] + ((uint32_t)buff[sz+7] << 8) + ((uint32_t)buff[sz+8] << 16) + ((uint32_t)buff[sz+9] << 24);
    //     p->count_us = raw_timestamp - timestamp_correction;
    //     p->crc = (uint16_t)buff[sz+10] + ((uint16_t)buff[sz+11] << 8);

    //     /* advance packet FIFO */
    //     gw_reg_write(LGW_RX_PACKET_DATA_FIFO_NUM_STORED, 0);
    // }

    // return nb_pkt_fetch;
	return 0;
}

int sx130x_hal_send(struct lgw_pkt_tx_s *pkt_data) 
{
    // int i, x;
    // uint8_t buff[256+TX_METADATA_NB]; /* buffer to prepare the packet to send + metadata before SPI write burst */
    // uint32_t part_int = 0; /* integer part for PLL register value calculation */
    // uint32_t part_frac = 0; /* fractional part for PLL register value calculation */
    // uint16_t fsk_dr_div; /* divider to configure for target datarate */
    // int transfer_size = 0; /* data to transfer from host to TX databuffer */
    // int payload_offset = 0; /* start of the payload content in the databuffer */
    // uint8_t pow_index = 0; /* 4-bit value to set the firmware TX power */
    // uint8_t target_mix_gain = 0; /* used to select the proper I/Q offset correction */
    // uint32_t count_trig = 0; /* timestamp value in trigger mode corrected for TX start delay */
    // bool tx_allowed = false;
    // uint16_t tx_start_delay;
    // bool tx_notch_enable = false;

	// c_printf(LOG_ALL, "TICK: %d\n", HAL_GetTick());

    // /* check if the concentrator is running */
    // if (lgw_is_started == false) {
    //     c_printf(LOG_ALL, "ERROR: CONCENTRATOR IS NOT RUNNING, START IT BEFORE SENDING\n");
    //     return LGW_HAL_ERROR;
    // }

    // /* check input range (segfault prevention) */
    // if (pkt_data->rf_chain >= LGW_RF_CHAIN_NB) {
    //     c_printf(LOG_ALL, "ERROR: INVALID RF_CHAIN TO SEND PACKETS\n");
    //     return LGW_HAL_ERROR;
    // }

    // /* check input variables */
    // if (rf_tx_enable[pkt_data->rf_chain] == false) {
    //     c_printf(LOG_ALL, "ERROR: SELECTED RF_CHAIN IS DISABLED FOR TX ON SELECTED BOARD\n");
    //     return LGW_HAL_ERROR;
    // }
    // if (rf_enable[pkt_data->rf_chain] == false) {
    //     c_printf(LOG_ALL, "ERROR: SELECTED RF_CHAIN IS DISABLED\n");
    //     return LGW_HAL_ERROR;
    // }
    // if (!IS_TX_MODE(pkt_data->tx_mode)) {
    //     c_printf(LOG_ALL, "ERROR: TX_MODE NOT SUPPORTED\n");
    //     return LGW_HAL_ERROR;
    // }
    // if (pkt_data->modulation == MOD_LORA) {
    //     if (!IS_LORA_BW(pkt_data->bandwidth)) {
    //         c_printf(LOG_ALL, "ERROR: BANDWIDTH NOT SUPPORTED BY LORA TX\n");
    //         return LGW_HAL_ERROR;
    //     }
    //     if (!IS_LORA_STD_DR(pkt_data->datarate)) {
    //         c_printf(LOG_ALL, "ERROR: DATARATE NOT SUPPORTED BY LORA TX\n");
    //         return LGW_HAL_ERROR;
    //     }
    //     if (!IS_LORA_CR(pkt_data->coderate)) {
    //         c_printf(LOG_ALL, "ERROR: CODERATE NOT SUPPORTED BY LORA TX\n");
    //         return LGW_HAL_ERROR;
    //     }
    //     if (pkt_data->size > 255) {
    //         c_printf(LOG_ALL, "ERROR: PAYLOAD LENGTH TOO BIG FOR LORA TX\n");
    //         return LGW_HAL_ERROR;
    //     }
    // } else if (pkt_data->modulation == MOD_FSK) {
    //     if((pkt_data->f_dev < 1) || (pkt_data->f_dev > 200)) {
    //         c_printf(LOG_ALL, "ERROR: TX FREQUENCY DEVIATION OUT OF ACCEPTABLE RANGE\n");
    //         return LGW_HAL_ERROR;
    //     }
    //     if(!IS_FSK_DR(pkt_data->datarate)) {
    //         c_printf(LOG_ALL, "ERROR: DATARATE NOT SUPPORTED BY FSK IF CHAIN\n");
    //         return LGW_HAL_ERROR;
    //     }
    //     if (pkt_data->size > 255) {
    //         c_printf(LOG_ALL, "ERROR: PAYLOAD LENGTH TOO BIG FOR FSK TX\n");
    //         return LGW_HAL_ERROR;
    //     }
    // } else {
    //     c_printf(LOG_ALL, "ERROR: INVALID TX MODULATION\n");
    //     return LGW_HAL_ERROR;
    // }

    // /* Enable notch filter for LoRa 125kHz */
    // if ((pkt_data->modulation == MOD_LORA) && (pkt_data->bandwidth == BW_125KHZ)) {
    //     tx_notch_enable = true;
    // }

    // /* Get the TX start delay to be applied for this TX */
    // tx_start_delay = lgw_get_tx_start_delay(tx_notch_enable, pkt_data->bandwidth);

    // /* interpretation of TX power */
    // for (pow_index = txgain_lut.size-1; pow_index > 0; pow_index--) {
    //     if (txgain_lut.lut[pow_index].rf_power <= pkt_data->rf_power) {
    //         break;
    //     }
    // }

    // /* loading TX imbalance correction */
    // target_mix_gain = txgain_lut.lut[pow_index].mix_gain;
    // if (pkt_data->rf_chain == 0) { /* use radio A calibration table */
    //     gw_reg_write(LGW_TX_OFFSET_I, cal_offset_a_i[target_mix_gain - 8]);
    //     gw_reg_write(LGW_TX_OFFSET_Q, cal_offset_a_q[target_mix_gain - 8]);
    // } else { /* use radio B calibration table */
    //     gw_reg_write(LGW_TX_OFFSET_I, cal_offset_b_i[target_mix_gain - 8]);
    //     gw_reg_write(LGW_TX_OFFSET_Q, cal_offset_b_q[target_mix_gain - 8]);
    // }

    // /* Set digital gain from LUT */
    // gw_reg_write(LGW_TX_GAIN, txgain_lut.lut[pow_index].dig_gain);

    // /* fixed metadata, useful payload and misc metadata compositing */
    // transfer_size = TX_METADATA_NB + pkt_data->size; /*  */
    // payload_offset = TX_METADATA_NB; /* start the payload just after the metadata */

    // /* metadata 0 to 2, TX PLL frequency */
    // switch (rf_radio_type[0]) { /* we assume that there is only one radio type on the board */
    //     case LGW_RADIO_TYPE_SX1255:
    //         part_int = pkt_data->freq_hz / (SX125x_32MHz_FRAC << 7); /* integer part, gives the MSB */
    //         part_frac = ((pkt_data->freq_hz % (SX125x_32MHz_FRAC << 7)) << 9) / SX125x_32MHz_FRAC; /* fractional part, gives middle part and LSB */
    //         break;
    //     case LGW_RADIO_TYPE_SX1257:
    //         part_int = pkt_data->freq_hz / (SX125x_32MHz_FRAC << 8); /* integer part, gives the MSB */
    //         part_frac = ((pkt_data->freq_hz % (SX125x_32MHz_FRAC << 8)) << 8) / SX125x_32MHz_FRAC; /* fractional part, gives middle part and LSB */
    //         break;
    //     default:
    //         break;
    // }

    // buff[0] = 0xFF & part_int; /* Most Significant Byte */
    // buff[1] = 0xFF & (part_frac >> 8); /* middle byte */
    // buff[2] = 0xFF & part_frac; /* Least Significant Byte */

    // /* metadata 3 to 6, timestamp trigger value */
    // /* TX state machine must be triggered at (T0 - lgw_i_tx_start_delay_us) for packet to start being emitted at T0 */
    // if (pkt_data->tx_mode == TIMESTAMPED)
    // {
    //     count_trig = pkt_data->count_us - (uint32_t)tx_start_delay + stm32_delay;
    //     buff[3] = 0xFF & (count_trig >> 24);
    //     buff[4] = 0xFF & (count_trig >> 16);
    //     buff[5] = 0xFF & (count_trig >> 8);
    //     buff[6] = 0xFF &  count_trig;
    // }

    // /* parameters depending on modulation  */
    // if (pkt_data->modulation == MOD_LORA) {
    //     /* metadata 7, modulation type, radio chain selection and TX power */
    //     buff[7] = (0x20 & (pkt_data->rf_chain << 5)) | (0x0F & pow_index); /* bit 4 is 0 -> LoRa modulation */

    //     buff[8] = 0; /* metadata 8, not used */

    //     /* metadata 9, CRC, LoRa CR & SF */
    //     switch (pkt_data->datarate) {
    //         case DR_LORA_SF7: buff[9] = 7; break;
    //         case DR_LORA_SF8: buff[9] = 8; break;
    //         case DR_LORA_SF9: buff[9] = 9; break;
    //         case DR_LORA_SF10: buff[9] = 10; break;
    //         case DR_LORA_SF11: buff[9] = 11; break;
    //         case DR_LORA_SF12: buff[9] = 12; break;
    //         default: break;
    //     }
    //     switch (pkt_data->coderate) {
    //         case CR_LORA_4_5: buff[9] |= 1 << 4; break;
    //         case CR_LORA_4_6: buff[9] |= 2 << 4; break;
    //         case CR_LORA_4_7: buff[9] |= 3 << 4; break;
    //         case CR_LORA_4_8: buff[9] |= 4 << 4; break;
    //         default: break;
    //     }
    //     if (pkt_data->no_crc == false) {
    //         buff[9] |= 0x80; /* set 'CRC enable' bit */
    //     } else {
    //         c_printf(LOG_ALL, "Info: packet will be sent without CRC\n");
    //     }

    //     /* metadata 10, payload size */
    //     buff[10] = pkt_data->size;

    //     /* metadata 11, implicit header, modulation bandwidth, PPM offset & polarity */
    //     switch (pkt_data->bandwidth) {
    //         case BW_125KHZ: buff[11] = 0; break;
    //         case BW_250KHZ: buff[11] = 1; break;
    //         case BW_500KHZ: buff[11] = 2; break;
    //         default: break;
    //     }
    //     if (pkt_data->no_header == true) {
    //         buff[11] |= 0x04; /* set 'implicit header' bit */
    //     }
    //     if (SET_PPM_ON(pkt_data->bandwidth,pkt_data->datarate)) {
    //         buff[11] |= 0x08; /* set 'PPM offset' bit at 1 */
    //     }
    //     if (pkt_data->invert_pol == true) {
    //         buff[11] |= 0x10; /* set 'TX polarity' bit at 1 */
    //     }

    //     /* metadata 12 & 13, LoRa preamble size */
    //     if (pkt_data->preamble == 0) { /* if not explicit, use recommended LoRa preamble size */
    //         pkt_data->preamble = STD_LORA_PREAMBLE;
    //     } else if (pkt_data->preamble < MIN_LORA_PREAMBLE) { /* enforce minimum preamble size */
    //         pkt_data->preamble = MIN_LORA_PREAMBLE;
    //         c_printf(LOG_ALL, "Note: preamble length adjusted to respect minimum LoRa preamble size\n");
    //     }
    //     buff[12] = 0xFF & (pkt_data->preamble >> 8);
    //     buff[13] = 0xFF & pkt_data->preamble;

    //     /* metadata 14 & 15, not used */
    //     buff[14] = 0;
    //     buff[15] = 0;

    //     /* MSB of RF frequency is now used in AGC firmware to implement large/narrow filtering in SX1257/55 */
    //     buff[0] &= 0x3F; /* Unset 2 MSBs of frequency code */
    //     if (pkt_data->bandwidth == BW_500KHZ) {
    //         buff[0] |= 0x80; /* Set MSB bit to enlarge analog filter for 500kHz BW */
    //     }

    //     /* Set MSB-1 bit to enable digital filter if required */
    //     if (tx_notch_enable == true) {
    //         c_printf(LOG_ALL, "INFO: Enabling TX notch filter\n");
    //         buff[0] |= 0x40;
    //     }
    // } 
	// else if (pkt_data->modulation == MOD_FSK) {
    //     /* metadata 7, modulation type, radio chain selection and TX power */
    //     buff[7] = (0x20 & (pkt_data->rf_chain << 5)) | 0x10 | (0x0F & pow_index); /* bit 4 is 1 -> FSK modulation */

    //     buff[8] = 0; /* metadata 8, not used */

    //     /* metadata 9, frequency deviation */
    //     buff[9] = pkt_data->f_dev;

    //     /* metadata 10, payload size */
    //     buff[10] = pkt_data->size;
    //     /* TODO: how to handle 255 bytes packets ?!? */

    //     /* metadata 11, packet mode, CRC, encoding */
    //     buff[11] = 0x01 | (pkt_data->no_crc?0:0x02) | (0x02 << 2); /* always in variable length packet mode, whitening, and CCITT CRC if CRC is not disabled  */

    //     /* metadata 12 & 13, FSK preamble size */
    //     if (pkt_data->preamble == 0) { /* if not explicit, use LoRa MAC preamble size */
    //         pkt_data->preamble = STD_FSK_PREAMBLE;
    //     } else if (pkt_data->preamble < MIN_FSK_PREAMBLE) { /* enforce minimum preamble size */
    //         pkt_data->preamble = MIN_FSK_PREAMBLE;
    //         c_printf(LOG_ALL, "Note: preamble length adjusted to respect minimum FSK preamble size\n");
    //     }
    //     buff[12] = 0xFF & (pkt_data->preamble >> 8);
    //     buff[13] = 0xFF & pkt_data->preamble;

    //     /* metadata 14 & 15, FSK baudrate */
    //     fsk_dr_div = (uint16_t)((uint32_t)LGW_XTAL_FREQU / pkt_data->datarate); /* Ok for datarate between 500bps and 250kbps */
    //     buff[14] = 0xFF & (fsk_dr_div >> 8);
    //     buff[15] = 0xFF & fsk_dr_div;

    //     /* insert payload size in the packet for variable mode */
    //     buff[16] = pkt_data->size;
    //     ++transfer_size; /* one more byte to transfer to the TX modem */
    //     ++payload_offset; /* start the payload with one more byte of offset */

    //     /* MSB of RF frequency is now used in AGC firmware to implement large/narrow filtering in SX1257/55 */
    //     buff[0] &= 0x7F; /* Always use narrow band for FSK (force MSB to 0) */

    // } 
	// else {
    //     c_printf(LOG_ALL, "ERROR: INVALID TX MODULATION..\n");
    //     return LGW_HAL_ERROR;
    // }

    // /* Configure TX start delay based on TX notch filter */
    // gw_reg_write(LGW_TX_START_DELAY, tx_start_delay);

    // /* copy payload from user struct to buffer containing metadata */
    // memcpy((void *)(buff + payload_offset), (void *)(pkt_data->payload), pkt_data->size);

    // /* reset TX command flags */
    // lgw_abort_tx();

    // /* put metadata + payload in the TX data buffer */
    // gw_reg_write(LGW_TX_DATA_BUF_ADDR, 0);
    // gw_reg_write_burst(LGW_TX_DATA_BUF_DATA, buff, transfer_size);

	// // TODO: add lbt 

    // // x = lbt_is_channel_free(&pkt_data, tx_start_delay, &tx_allowed);
    // // if (x != LGW_LBT_SUCCESS) {
    // //     c_printf(LOG_ALL, "ERROR: Failed to check channel availability for TX\n");
    // //     return LGW_HAL_ERROR;
    // // }

    // // if (tx_allowed == true) {
	// if (true) {
    //     switch(pkt_data->tx_mode) {
    //         case IMMEDIATE:
    //             gw_reg_write(LGW_TX_TRIG_IMMEDIATE, 1);
    //             break;

    //         case TIMESTAMPED:
    //             gw_reg_write(LGW_TX_TRIG_DELAYED, 1);
    //             break;

    //         case ON_GPS:
    //             gw_reg_write(LGW_TX_TRIG_GPS, 1);
    //             break;

    //         default:
    //             return LGW_HAL_ERROR;
    //     }
    // } else {
    //     c_printf(LOG_ALL, "ERROR: Cannot send packet, channel is busy (LBT)\n");
    //     return LGW_HAL_ERROR;
    // }

	// c_printf(LOG_ALL, "Delay: %d\n", stm32_delay);
	// stm32_delay += 2;

    return LGW_HAL_SUCCESS;
}

int sx130x_hal_status(uint8_t select, uint8_t *code) 
{
    // int32_t read_value;

    // if (select == TX_STATUS) 
	// {
    //     gw_reg_read(LGW_TX_STATUS, &read_value);
    //     if (lgw_is_started == false) 
	// 	{
    //         *code = TX_OFF;
    //     } 
	// 	else if ((read_value & 0x10) == 0) 
	// 	{ 
	// 		/* bit 4 @1: TX programmed */
    //         *code = TX_FREE;
    //     } 
	// 	else if ((read_value & 0x60) != 0) 
	// 	{ 
	// 		/* bit 5 or 6 @1: TX sequence */
    //         *code = TX_EMITTING;
    //     } 
	// 	else 
	// 	{
    //         *code = TX_SCHEDULED;
    //     }
    //     return LGW_HAL_SUCCESS;

    // } 
	// else if (select == RX_STATUS) 
	// {
    //     *code = RX_STATUS_UNKNOWN;
    //     return LGW_HAL_SUCCESS;
    // } 
	// else 
	// {
    //     return LGW_HAL_ERROR;
    // }
	return 0;
}

int sx130x_hal_get_trigcnt(uint32_t* trig_cnt_us) 
{
    // int i;
    // int32_t val;

	// i = gw_reg_read(LGW_TIMESTAMP, &val);

    // if(i == LGW_REG_SUCCESS)
	// {
    //     *trig_cnt_us = (uint32_t)val;
    //     return LGW_HAL_SUCCESS;
    // } 
	// else 
	// {
    //     return LGW_HAL_ERROR;
    // }

	return 0;
}

static int32_t gw_hal_bw_getval(int x) 
{
    switch (x) {
        case BW_500KHZ:
            return 500000;
        case BW_250KHZ:
            return 250000;
        case BW_125KHZ:
            return 125000;
        case BW_62K5HZ:
            return 62500;
        case BW_31K2HZ:
            return 31200;
        case BW_15K6HZ:
            return 15600;
        case BW_7K8HZ :
            return 7800;
        default:
            return -1;
    }
}

static int load_firmware(uint8_t target, uint8_t *firmware, uint16_t size) 
{
    // int reg_rst;
    // int reg_sel;
    
	// if (target == MCU_ARB) 
	// {
	// 	if (size != MCU_ARB_FW_BYTE) 
	// 	{
    //         return -1;
    //     }
    //     reg_rst = LGW_MCU_RST_0;
    //     reg_sel = LGW_MCU_SELECT_MUX_0;
	// } 
	// else if (target == MCU_AGC) 
	// {
	// 	if (size != MCU_AGC_FW_BYTE) 
	// 	{
    //         return -1;
    //     }
    //     reg_rst = LGW_MCU_RST_1;
    //     reg_sel = LGW_MCU_SELECT_MUX_1;
	// } 
	// else 
	// {
    //     return -1;
    // }

    // /* reset the targeted MCU */
    // gw_reg_write(reg_rst, 1);

    // /* set mux to access MCU program RAM and set address to 0 */
    // gw_reg_write(reg_sel, 0);
    // gw_reg_write(LGW_MCU_PROM_ADDR, 0);

    // /* write the program in one burst */
    // gw_reg_write_burst(LGW_MCU_PROM_DATA, firmware, size);

    // /* give back control of the MCU program ram to the MCU */
    // gw_reg_write(reg_sel, 1);

    return 0;
}

static int load_firmware_sd(uint8_t target, char *filename, uint16_t size)
{
	// uint8_t *firmware;
	// int result = LGW_HAL_ERROR;

	// firmware = (uint8_t *)pvPortMalloc(size);
	// if(firmware != NULL)
	// {
	// 	if(config_load_firmware(filename, firmware, size) == true)
	// 	{
	// 		if(load_firmware(target, firmware, size) == LGW_HAL_SUCCESS)
	// 		{
	// 			result = LGW_HAL_SUCCESS;
	// 		}
	// 	}
	// }

	// vPortFree((void *)firmware);

	// return result;
	return 0;
}

static uint16_t lgw_get_tx_start_delay(bool tx_notch_enable, uint8_t bw) 
{
    // float notch_delay_us = 0.0;
    // float bw_delay_us = 0.0;
    // float tx_start_delay;

    // /* Calibrated delay brought by SX1301 depending on signal bandwidth */
    // switch (bw) {
    //     case BW_125KHZ:
    //         bw_delay_us = 1.5;
    //         break;
    //     case BW_500KHZ:
    //         /* Intended fall-through: it is the calibrated reference */
    //     default:
    //         break;
    // }

    // tx_start_delay = (float)TX_START_DELAY_DEFAULT - bw_delay_us;

    // return (uint16_t)tx_start_delay; /* keep truncating instead of rounding: better behaviour measured */
	return 0;
}

static int lgw_abort_tx(void) 
{
    // int i;

    // i = gw_reg_write(LGW_TX_TRIG_ALL, 0);

    // if (i == LGW_REG_SUCCESS) return LGW_HAL_SUCCESS;
    // else return LGW_HAL_ERROR;
	return 0;
}

static void gw_constant_adjust(void) 
{
	// gw_reg_write(LGW_RSSI_BB_FILTER_ALPHA,6); /* default 7 */
	// gw_reg_write(LGW_RSSI_DEC_FILTER_ALPHA,7); /* default 5 */
	// gw_reg_write(LGW_RSSI_CHANN_FILTER_ALPHA,7); /* default 8 */
	// gw_reg_write(LGW_RSSI_BB_DEFAULT_VALUE,23); /* default 32 */
	// gw_reg_write(LGW_RSSI_CHANN_DEFAULT_VALUE,85); /* default 100 */
	// gw_reg_write(LGW_RSSI_DEC_DEFAULT_VALUE,66); /* default 100 */
	// gw_reg_write(LGW_DEC_GAIN_OFFSET,7); /* default 8 */
	// gw_reg_write(LGW_CHAN_GAIN_OFFSET,6); /* default 7 */
	
	// gw_reg_write(LGW_SNR_AVG_CST,3); /* default 2 */
	// if (lorawan_public) { /* LoRa network */
	// 	gw_reg_write(LGW_FRAME_SYNCH_PEAK1_POS,3); /* default 1 */
	// 	gw_reg_write(LGW_FRAME_SYNCH_PEAK2_POS,4); /* default 2 */
	// } else { /* private network */
	// 	gw_reg_write(LGW_FRAME_SYNCH_PEAK1_POS,1); /* default 1 */
	// 	gw_reg_write(LGW_FRAME_SYNCH_PEAK2_POS,2); /* default 2 */
	// }
	
	// if (lorawan_public) { /* LoRa network */
	// 	gw_reg_write(LGW_MBWSSF_FRAME_SYNCH_PEAK1_POS,3); /* default 1 */
	// 	gw_reg_write(LGW_MBWSSF_FRAME_SYNCH_PEAK2_POS,4); /* default 2 */
	// } else {
	// 	gw_reg_write(LGW_MBWSSF_FRAME_SYNCH_PEAK1_POS,1); /* default 1 */
	// 	gw_reg_write(LGW_MBWSSF_FRAME_SYNCH_PEAK2_POS,2); /* default 2 */
	// }
	
	// /* Improvement of reference clock frequency error tolerance */
	// gw_reg_write(LGW_ADJUST_MODEM_START_OFFSET_RDX4, 1); /* default 0 */
	// gw_reg_write(LGW_ADJUST_MODEM_START_OFFSET_SF12_RDX4, 4094); /* default 4092 */
	// gw_reg_write(LGW_CORR_MAC_GAIN, 7); /* default 5 */
	
	// /* FSK datapath setup */
	// gw_reg_write(LGW_FSK_RX_INVERT,1); /* default 0 */
	// gw_reg_write(LGW_FSK_MODEM_INVERT_IQ,1); /* default 0 */
	
	// /* FSK demodulator setup */
	// gw_reg_write(LGW_FSK_RSSI_LENGTH,4); /* default 0 */
	// gw_reg_write(LGW_FSK_PKT_MODE,1); /* variable length, default 0 */
	// gw_reg_write(LGW_FSK_CRC_EN,1); /* default 0 */
	// gw_reg_write(LGW_FSK_DCFREE_ENC,2); /* default 0 */
	// gw_reg_write(LGW_FSK_ERROR_OSR_TOL,10); /* default 0 */
	// gw_reg_write(LGW_FSK_PKT_LENGTH,255); /* max packet length in variable length mode */
	// gw_reg_write(LGW_FSK_PATTERN_TIMEOUT_CFG,128); /* sync timeout (allow 8 bytes preamble + 8 bytes sync word, default 0 */
	
	// /* TX general parameters */
	// gw_reg_write(LGW_TX_START_DELAY, TX_START_DELAY_DEFAULT); /* default 0 */
	
	// /* TX LoRa */
	// gw_reg_write(LGW_TX_SWAP_IQ,1); /* "normal" polarity; default 0 */
	// if (lorawan_public) { /* LoRa network */
	// 	gw_reg_write(LGW_TX_FRAME_SYNCH_PEAK1_POS,3); /* default 1 */
	// 	gw_reg_write(LGW_TX_FRAME_SYNCH_PEAK2_POS,4); /* default 2 */
	// } else { /* Private network */
	// 	gw_reg_write(LGW_TX_FRAME_SYNCH_PEAK1_POS,1); /* default 1 */
	// 	gw_reg_write(LGW_TX_FRAME_SYNCH_PEAK2_POS,2); /* default 2 */
	// }
	
	// /* TX FSK */
	// gw_reg_write(LGW_FSK_TX_GAUSSIAN_SELECT_BT,2); /* Gaussian filter always on TX, default 0 */
}
