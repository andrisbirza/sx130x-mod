#include "sx130x_ioctl.h"
#include "sx130x_hal.h"

#include <asm-generic/errno-base.h>

int sx130x_ioctl_conf_set_board(unsigned int cmd, unsigned long arg)
{
	struct sx130x_conf_board board_cfg;

	if(copy_from_user(&board_cfg, arg, sizeof(struct sx130x_conf_board)))
	{
		return -EFAULT;
	}

	pr_debug("sx130x: set board config\n");
	pr_debug("sx130x: - public: %d\n", board_cfg.lorawan_public);
	pr_debug("sx130x: - clksrc: %d\n", board_cfg.clksrc);

	if(sx130x_hal_board_setconf(&board_cfg))
	{
		return -EINVAL;
	}

	return 0;
}
